module SortingAlgorithms where
-- Some basic sorting algrothims will be implemented in this file --

-- 1. Quicksort --
quicksort1 :: (Ord a) => [a] -> [a]
quicksort1 [] = []
quicksort1 (x:xs) =
  let smallerSorted = quicksort1 [a | a <- xs, a <= x]
      biggerSorted = quicksort1 [a | a <- xs, a > x]
  in  smallerSorted ++ [x] ++ biggerSorted

-- 1.1. In-place Quicksort --

-- 2. Bubblesort --

-- 3. Mergesort --
-- merge takes two lists and merges them, preserving duplicates
merge :: Ord a => [a] -> [a] -> [a]
merge xs [] = xs
merge [] ys = ys
merge (x:xs) (y:ys)
    | x <= y = x:merge xs (y:ys)
    | otherwise = y:merge (x:xs) ys
    
-- 3.1. top-down mergesort --
tdMergeSort :: Ord a => [a] -> [a]
tdMergeSort [] = []
tdMergeSort [a] = [a]
tdMergeSort xs = merge (tdMergeSort (firstHalf xs)) (tdMergeSort (secondHalf xs))

firstHalf xs = let { n = length xs} in take (div n 2) xs
secondHalf xs = let { n = length xs} in drop (div n 2) xs

-- 3.2. bottom-up mergesort --
buMergeSort :: Ord a => [a] -> [a]
buMergeSort [] = []

-- 4. Insertionsort --
insert :: Ord a => a -> [a] -> [a]
insert x [] = [x]
insert x (y:ys)
    | x < y = x:y:ys
    | otherwise = y:(insert x ys)
    
insertionSort :: Ord a => [a] -> [a]
insertionSort [] = []
insertionSort (x:xs) = insert x (insertionSort xs)

-- 5.  Selectionsort --
removeElement :: Eq a => a -> [a] -> [a]
removeElement _ [] = []
removeElement y (x:xs)
    | y == x = xs
    | otherwise = x : removeElement y xs
    
(<<==) :: Ord a => a -> [a] -> Bool
(<<==) a [] = True
(<<==) a (x:xs)
    | a <= x = a <<== xs
    | otherwise = False
    
minValue :: Ord a => [a] -> a
minValue [] = undefined
minValue (x : xs) = case x <<== xs of 
                         True -> x 
                         False -> minValue xs
                         
selectionSort :: (Eq a, Ord a) => [a] -> [a]
selectionSort [] = []
selectionSort xs = s : selectionSort (removeElement s xs)
        where s = minValue xs